// TODO specify multiple key/command pairs, spawn a thread per device to handle events :( :p
/* onkey.c - linux event handler that runs an external command */

/*
  Copyright 2018 by Craig Comstock <craig@unreasonablefarm.org>

  This file is part of shelli.

  shelli is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  shelli is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with shelli.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <dirent.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <linux/input.h>
#include <linux/input-event-codes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

// borrowed from evtest.c
#define BITS_PER_LONG (sizeof(long) * 8)
#define NBITS(x) (((x)-1/BITS_PER_LONG)+1)
#define OFF(x) ((x)%BITS_PER_LONG)
#define BIT(x) (1UL<<OFF(x))
#define LONG(x) ((x)/BITS_PER_LONG)
#define test_bit(bit, array) ((array[LONG(bit)] >> OFF(bit)) & 1)


static int is_event_path(const struct dirent *dir)
{
  return strncmp("event", dir->d_name, 5) == 0;
}

int get_event_path(char *event_path, int desired_code)
{
  struct dirent **namelist;
  int i, ndev, devnum;
  int rc = -1;

  ndev = scandir("/dev/input", &namelist, is_event_path, alphasort);
  if (ndev <= 0)
  {
    fprintf(stderr, "Unable to list any device event paths in /dev/input/event*\n");
    return rc;
  }

  for (i = 0; i < ndev; i++)
  {
    if (rc == 0)
    {
      free(namelist[i]);
      continue;
    }

    int fd = -1;
    unsigned int type;
    unsigned long bit[EV_MAX][NBITS(KEY_MAX)];
    int abs[6] = {0};

    snprintf(event_path, PATH_MAX, "%s/%s", "/dev/input", namelist[i]->d_name);

    fd = open(event_path, O_RDONLY);
    if (fd < 0)
      continue;
    memset(bit, 0, sizeof(bit));
    ioctl(fd, EVIOCGBIT(0, EV_MAX), bit[0]);
    if (test_bit(EV_KEY, bit[0])) {
      ioctl(fd, EVIOCGBIT(EV_KEY, KEY_MAX), bit[EV_KEY]);
      if (test_bit(desired_code, bit[EV_KEY])) {
        rc = 0;
      }
    }
    close(fd);

    free(namelist[i]);
  }
  return rc;
}
int main(int argc, char *const *argv)
{
  int c;
  int digit_optind = 0;
  int eventcode = -1;
  char *command = NULL;
  char device[PATH_MAX] = "";

  while (1) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    static struct option long_options[] = {
        {"eventcode", required_argument, 0, 'e'},
        {"device", required_argument, 0, 'd'},
        {"command", required_argument, 0, 'c'},
        {0, 0, 0, 0}};

    c = getopt_long(argc, argv, "e:c:d:", long_options, &option_index);
    if (c == -1)
      break;

    fprintf(stderr,"getopt character code %c\n", c);
    fprintf(stderr,"option_index %d\n", option_index);
    switch (c) {
    case 'e':
      eventcode = atoi(optarg);
      fprintf(stderr,"eventcode is %d\n", eventcode);
      break;
    case 'c':
      command = optarg;
      fprintf(stderr,"command is '%s'\n", command);
      break;
    case 'd':
      strcpy(device, optarg);
      fprintf(stderr,"device path is '%s'\n", device);
      break;
    default:
      fprintf(stderr,"?? getopt returned character code 0%o ??\n", c);
    }
  }

  if (optind < argc) {
    fprintf(stderr,"non-option ARGV-elements: ");
    while (optind < argc)
      fprintf(stderr,"%s ", argv[optind++]);
    fprintf(stderr,"\n");
  }

  if (device[0] == 0 && eventcode != -1)
  {
    int rc = get_event_path(device, eventcode);
    if (rc != 0)
    {
      printf("Unable to find key event device path. Specify as arguments instead.\n");
    }
  }
  if (device[0] == 0 || command == NULL)
  {
    printf("Usage: onkey -e <event code> -d <device path> -c <command>\n");
    exit(1);
  }

  int fd;
  int rs;
  int screen_state = 1;

  fd = open(device, O_RDONLY);
  struct input_event ev;

  while ((rs = read(fd, &ev, sizeof(struct input_event))) != -1) {
fprintf(stderr,"ev.code %d ev.value %d\n", ev.code, ev.value);
    if (ev.code == eventcode && ev.value == 1) {
      system(command);
    }
  }

  close(fd);

  exit(EXIT_SUCCESS);
}
