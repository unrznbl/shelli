#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/input.h>


int main(int argc, char** argv)
{
  struct ff_effect effect;
  struct input_event play, stop, gain;
  int fd;
  const char * device_file_name = "";
  if (argc == 2)
  {
    device_file_name = argv[1];
  }
else
{
  fprintf(stderr, "Usage: vibrate <device path>\n");
  exit(2);
}

  fd = open(device_file_name, O_RDWR);
  if (fd == -1) {
    perror("Open device file");
    exit(1);
  }

  memset(&effect,0,sizeof(effect));
  effect.type = FF_RUMBLE;
  effect.id = -1;
//  effect.u.rumble.strong_magnitude = 0;
//  effect.u.rumble.weak_magnitude = 0xc000;
effect.u.rumble.strong_magnitude = 0x8000;
effect.u.rumble.weak_magnitude = 0;
  effect.replay.length = 5000;
  effect.replay.delay = 0;

  if (ioctl(fd, EVIOCSFF, &effect) == -1) {
    perror("Error uploading weak rumble effect");
    exit(1);
  } else {
//    printf("OK, weak rumble effect is id: %d\n", effect.id);
  }
  memset(&play,0,sizeof(play));
  play.type = EV_FF;
  play.code = effect.id;
  play.value = 1;

  if (write(fd, (const void*) &play, sizeof(play)) == -1) {
    perror("play effect");
    exit(1);
  }

  sleep(1);

  memset(&stop,0,sizeof(stop));
  stop.type = EV_FF;
  stop.code = effect.id;
  stop.value = 0;
  if (write(fd, (const void*) &stop, sizeof(stop)) == -1) {
    perror("stop effect");
    exit(1);
  }
  exit(0);
}
