Use command `calls` to check current voice call status at various times.
Make a voice call
Ensure that earpiece, headphones, speaker, mute and unmute all do the proper things
Hangup the call with hangup
Answer a voice call with answer
Add a number <tab> contact name in $HOME/Contacts
Text that contact with `text <partial contact name> <message>`
Text with `sms <number> <message>`
Turn phone off with `off`
Check that power button turns screen off and that gestures written when screen is off are not entered into console.
