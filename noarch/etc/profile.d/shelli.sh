#echo "adding command not found handle"
command_not_found_handle() {
#  echo "argc $#, args $@"
  if expr '(' "$1" : "[0-9\#\*\+-]*$" ')' = '(' length "$1" ')' >/dev/null; then
    confirm "dial $1" && dial "$1"
  else
    search "$@" || echo "command not found: $@"
  fi
}
